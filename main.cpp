#include <iostream>
#include <sstream>
#include <string>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include <nlohmann/json.hpp>
#include <random>

using json = nlohmann::json;

// Unfortunate global variables
std::string fileName;
std::string newFileName;
std::string newImage;
std::string format;

// Functions
void getIMG();
void fileNames();
cv::Mat applyFilter(int selection, cv::Mat filtered);
void editImage();
int get_random_number();


int main(int, char**) 
{
    // Call getIMG function
    getIMG();
    // Call fileNames function
    fileNames();
    // Call editImage function
    editImage();
}

//random number function for filters between 1 and 6
int get_random_number()
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(1,6);

    int random = dist6(rng);
    //returns the random number
    return random;
};


void getIMG()
{   
    // Constant string for image download system command
    const std::string cmd("wget -P ../opencv-project/original/ ");

    // Get image url
    RestClient::Response getImage = RestClient::get(
    "https://aws.random.cat/meow");

    // Parse url body
    auto json_body = json::parse(getImage.body);

    // Save image url in to variable
    newImage = json_body["file"];

    // Get file format and save it in to variable so that edited image can be saved in same format
    unsigned delim = newImage.find_last_of(".");
    format = newImage.substr (delim);

    // Check if format is .gif and exit if it is because we do not like those
    if (format == ".gif")
    {
        std::cout << "We do not deal with .gifs, sorry." << std::endl;

        exit(EXIT_SUCCESS);
    }

    // Add image download system command in front of image url
    newImage = cmd + newImage;

    // Execute wget system command using newImage variable
    system(newImage.c_str());

}

void fileNames()
{
    // Delimiters to use with getting file name
    unsigned first = newImage.find_last_of("/");
    unsigned last = newImage.find_last_of(".");
    first++;

    // Get file name and create new file name and save in to variables
    fileName = newImage.substr (first);
    newFileName = newImage.substr (first, last-first);
    newFileName = newFileName + "_edited" + format;

    // Print out original and new file names
    std::cout << fileName << std::endl;
    std::cout << newFileName << std::endl;
}

cv::Mat applyFilter(int selection, cv::Mat img)
{
    cv::Mat filtered;

    if (selection == 1)
    {
       cv::cvtColor(img, filtered, cv::COLOR_BGR2GRAY);
    }
    if (selection == 2)
    {
        cv::equalizeHist(img, filtered);
    }
    if (selection == 3)
    {
        cv::threshold(img, filtered, 127, 255, cv::THRESH_BINARY);
    }
    if (selection == 4)
    {
        cv::copyMakeBorder(img, filtered, 25, 25, 25, 25, 4, 1);
    }
    if (selection == 5)
    {
        cv::blur (img, filtered, cv::Size(3,3));
    }
    if (selection == 6)
    {
        cv::GaussianBlur (img, filtered, cv::Size(3,3), 2);
    }
    return filtered;
}

void editImage()
{   
    float width = 0.0;
    float height = 0.0;
    float divider = 0.0;
    int newHeight;
    int newWidth;
    int a = 0, b = 0;

    // File path constants
    const std::string origpath("../opencv-project/original/");
    const std::string editpath("../opencv-project/output/");

    // File path to image to be edited
    std::string edit = origpath + fileName;

    // Create Mat variable of the image
    cv::Mat image = cv::imread(edit);

    // Save image dimensions in to variables
    width = image.size().width;
    height = image.size().height;

    // Check if image size is in requested limits and create variables to convert it if it is not
    if (width > 300 || height > 300)
    {
        if (width > height)
        {
            newWidth = 300;
            divider = width / 300;
            newHeight = height / divider;
        }
        else if (width < height)
        {
            newHeight = 300;
            divider = height / 300;
            newWidth = width / divider;
        }
        else
        {
            newHeight = 300;
            newWidth = 300;
        }
    }
    else
    {
        newWidth = width;
        newHeight = height;
    }

    // Print out old and new image size and divider used in the conversion process
    std::cout << image.size().width << std::endl;
    std::cout << image.size().height << std::endl;
    std::cout << newWidth << std:: endl;
    std::cout << newHeight << std::endl;
    std::cout << divider << std::endl;


     // Create Mat variables of wanted editing steps
    cv::Mat resize, filter1, filter2;

    // Resize image with values given by if statement
    cv::resize(image, resize, cv::Size(newWidth, newHeight), cv::INTER_LINEAR);


    // Generate two random numbers to use with applyFilter function
    while ((a == b)||(a == 2)||(b == 2 && a != 1))
    {
        a = get_random_number();
        b = get_random_number();
    }


    // Call applyFilter function to add filters to image
    filter1 = applyFilter(a, resize);
    filter2 = applyFilter(b, filter1);

    // Save edited image
    std::string edited = editpath + newFileName;
    cv::imwrite(edited, filter2);
}

