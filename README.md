## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title
OpenCV

### Short Description
Gets random cat images json file from https://aws.random.cat/meow.  
Then the image is filtered and rezized to our specifications: 300 x 300 pixels.  
After which two random filters are applies from a list of 6 filters.

If the json package is .gif then the program is closed.  

### Install
Needed:  
gcc version 11.1.0  
cmake version 3.10  
nlohmann_json  
restclient-cpp  
opencv2  


### Usage
While in root folder  
build it with cmake  
$ make  
$ ./main

### Maintainer(s)

Tuomas Uusi-Luomalahti @slupato  
Antti Lehtosalo @mediator246  

### Contributing

Tuomas Uusi-Luomalahti @slupato  
Antti Lehtosalo @mediator246  

### License

Free to use
